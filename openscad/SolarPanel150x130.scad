// Author: Olivier Lenoir - <olivier.len02@gmail.com>
// Created: 2022-05-16
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2019.01-RC2
// Project: Solar Panel 150 x 130
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [110, 100, 10];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 775;  // camera distance
$vpf = undef;  // camera field of view


// include


// use
use <../submodule/NotchBox/openscad/NotchBox.scad>;


// Main
// Corrugated fiberboard
// Thickness 2.8mm
// Speed 1200mm/min, Power 75%, Pass 7
// Test: projection(cut = false) notch_box(55, 34, 21, 2.8, 6);

projection(cut = false)
difference() {
    notch_box(170, 150, 21, 2.8, 6);
    translate([170 / 2, 150 / 2])
        cube([150 - 4, 130 - 4, 9], center = true);
}


// Modules


// Functions

